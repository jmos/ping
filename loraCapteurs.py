import serial
import time
import loraLib as lora
from datetime import datetime
import json
import pickle

port = serial.Serial('/dev/ttyUSB2', 38400)
print('Port {} ouvert'.format(port.name))
print('*****************************')

lora.init(port)
lora.modeRX()

try:
    while True:
        line = port.readline()
        if not line:
            continue

        try:
            msg = line.decode().split(',')[4]
            msg = msg.replace('\r', '')
            msg = msg.replace('\n', '')
            msg = bytearray.fromhex(msg).decode()
        except IndexError:
            #ligne \r\n
            continue
        except UnicodeDecodeError:
            continue
        except ValueError:
            #ligne \r\n
            continue

        if msg == lora.CONST_DATA_SWITCH_MODE:
            print('Recu : ', msg)

            with open('/home/pi/Desktop/donneesCapteurs', 'rb') as reporting:
                lecture = pickle.Unpickler(reporting)
                donnees_recupere = lecture.load()
            """
            data = json.dumps({"d": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                               "g": "NE",
                               "v": "12.2",
                               "e": "4",
                               "h": "12.2",
                               "l": "12.2",
                               "p": "12.2",
                               "t": "86.7",
                               "w": "12.62",
                               "i": "12.62",
                               "u": "12.62",
                               "f": "1"},
                              separators=('.', ':'))
            """
            data = json.dumps({"d": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                               "g": donnees_recupere['girouette'],
                               "v": donnees_recupere['vent'],
                               "e": donnees_recupere['encodeur'],
                               "h": donnees_recupere['humidite'],
                               "l": donnees_recupere['luminosite'],
                               "p": donnees_recupere['pluie'],
                               "t": donnees_recupere['temperature'],
                               "w": donnees_recupere['puissance'],
                               "i": donnees_recupere['courant'],
                               "u": donnees_recupere['tension']}, separators=('.', ':'))


            lora.stopRX()
            lora.TX(data)
            lora.modeRX()
        else:
            print('Recu : ', msg)
            try:
                etatEolienne = msg.split(':')[1]
                # print(etatEolienne)
                with open('/home/pi/Desktop/eolienne', 'w') as file:
                    if etatEolienne == '0':
                        #print('Eolienne arrêt')
                        file.write('OFF')
                    elif etatEolienne == '1':
                        #print('Eolienne en marche')
                        file.write('ON')

            except IndexError:
                pass

except KeyboardInterrupt:
    port.close()
    print('\n*****************************')
    print('Port fermé')
