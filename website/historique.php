<!DOCTYPE html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Historique</title>

    <link rel="stylesheet" href="css/bootstrap.css"/> 
    <link rel="stylesheet" href="css/projet.css"/>
</head>

<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <!--
      <a class="navbar-brand mr-auto mr-lg-0" href="#">Navigation</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>
	-->
	    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
	    	<ul class="navbar-nav mr-auto">
		        <li class="nav-item">
		        	<a class="nav-link" href="index.php">Données en direct</a>
		        </li>
		        <li class="nav-item active">
		        	<a class="nav-link" href="historique.php">Historique</a>
		        </li>
	       	</ul>
	    </div>
	</nav>

    <main role="main" class="container">
    	<?php include('bandeau.inc.php') ?>

        <div class="container">
            <div class="row">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Puissance (mW)</th>
                            <th>Direction vent</th>
                            <th>Vitesse vent (km/h)</th>
                            <th>Température (°C)</th>
                            <th>Humidité (%)</th>
                            <th>Luminosité (lux)</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        include("connexionSQL.inc.php");

                        $requete = $bdd -> query('SELECT * FROM mesures ORDER BY date DESC');
                        
                        while( $donnees = $requete->fetch() ){
                        ?>
                        <tr>
                            <td><?php echo $donnees['date'] ?></td>
                            <td><?php echo $donnees['puissance'] ?></td>
                            <td><?php echo $donnees['vent_direction'] ?></td>
                            <td><?php echo $donnees['vent'] ?></td>
                            <td><?php echo $donnees['temperature'] ?></td>
                            <td><?php echo $donnees['humidite'] ?></td>
                            <td><?php echo $donnees['luminosite'] ?></td>
                            <!-- AJOUTER COURANT/TENSION/PUISSANCE -->
                        </tr>
                        <?php
                        }
                        $requete->closeCursor();
                        ?>  
                                                   
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/holderjs@2.9.4/holder.js"></script>
    <script src="offcanvas.js"></script>
  

	<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" preserveAspectRatio="none" style="display: none; visibility: hidden; position: absolute; top: -100%; left: -100%;"><defs><style type="text/css"></style></defs><text x="0" y="2" style="font-weight:bold;font-size:2pt;font-family:Arial, Helvetica, Open Sans, sans-serif">32x32</text></svg>
</body>
</html>

<script>
    window.setInterval('refresh()', 5000);

    function refresh() {
        window.location.reload();
    }
</script>