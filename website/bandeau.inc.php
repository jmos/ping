<div class="row p-3 my-3 text-white-50 bg-purple rounded box-shadow">
    <div class="col-3">
        <?php
            require_once("connexionSQL.inc.php");

            $requete = $bdd -> query('SELECT date, encodeur FROM mesures ORDER BY date DESC');

            $data = $requete->fetch();
        ?>

        <h6 class="mb-0 text-white lh-10">Station météo n° <?php echo $data['encodeur'] ?></h6>
        <small>Dernière mesures : <?php echo $data['date'] ?></small>
    </div>

    <div class="col-5 text-white">
        <form action="update_temps.php" method="post">
            <div class="row">
                <div class="col-sm-7" style="padding: 0px;">
                    <label for="temps" class="col-form-label">Rafraîchissement données:</label>
                </div>
                <div class="col-sm-3" style="padding: 0px;">
                    <input type="text" class="form-control" name="temps" id="temps" placeholder="sec">
                </div>
                <div class="col-sm-2" style="padding: 0px;">
                    <input class="btn btn-success" type="submit" value="OK"/> 
                </div>
            </div>
        </form>

        <div class="row mt-2">
            <label class="mr-2"> Forcer rafraîchissement données : </label>
            <a href="update_donnees.php">
                <img border="0"src="images/refresh.png" width="25" height="25">
            </a>
        </div>

    </div>

    <div class="col-4 float-right">
        <?php
            $requete->closeCursor();

       		include("connexionSQL.inc.php");
            $requete2 = $bdd -> query('SELECT fonctionnement FROM flag WHERE id = 1');
            $data2 = $requete2->fetch();
            
            if ($data2['fonctionnement'] == 1):
        ?>

        <b class="text-white">Eolienne en marche</b>
        <a href="eolienne_marche.php" class="btn btn-success" role="button">ON</a>
        <a href="eolienne_arret.php" class="btn btn-secondary" role="button">OFF</a>
        
        <?php 
            else:
        ?>

        <b class="text-white">Eolienne à l'arrêt</b>
        <a href="eolienne_marche.php" class="btn btn-secondary" role="button">ON</a>
        <a href="eolienne_arret.php" class="btn btn-danger" role="button">OFF</a>
        
        <?php 
            endif;
        ?>
    </div>  

</div>

<?php $requete2->closeCursor(); ?>