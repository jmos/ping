<?php 

	$temps = $_POST['temps'];
	
	if(ctype_digit($temps) == true) {
		include('connexionSQL.inc.php');

		$update = $bdd -> prepare('UPDATE flag SET update_temps = ? WHERE id = 1');
		$update -> execute(array($temps));

		header('Location: ' . $_SERVER['HTTP_REFERER']);
	} else {
		echo "Le temps entré n'est pas un entier, retournez sur page d'acceuil";
	}

?>