<!DOCTYPE html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Données en direct</title>

    <link rel="stylesheet" href="css/bootstrap.css"/> 
    <link rel="stylesheet" href="css/weather-icons.min.css"/>
    <link rel="stylesheet" href="css/weather-icons-wind.min.css"/>
    <link rel="stylesheet" href="css/projet.css"/>
</head>

<body class="bg-light">
	<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
	    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
	    	<ul class="navbar-nav mr-auto">
		        <li class="nav-item active">
		        	<a class="nav-link" href="index.php">Données en direct <span class="sr-only">(current)</span></a>
		        </li>
		        <li class="nav-item">
		        	<a class="nav-link" href="historique.php">Historique</a>
		        </li>
	       	</ul>
	    </div>
	</nav>

    <main role="main" class="container">
    	<?php include('bandeau.inc.php'); 

        include("connexionSQL.inc.php");

        $requete = $bdd -> query('SELECT * FROM mesures ORDER BY date DESC');

        $data = $requete->fetch();

        $vent_direction = $data['vent_direction'];
        $vent = $data['vent'];
        $puissance = $data['puissance'];
        $temperature = $data['temperature'];
        $humidite = $data['humidite'];
        $luminosite = $data['luminosite'];
        $pluie = $data['pluie'];
        $courant = $data['courant'];
        $tension = $data['tension'];

        $requete->closeCursor();

        //calculer l'angle en fonction de la direction
        switch ($vent_direction) {
            case 'N':
                $angle = 0;
                break;
            case 'NE':
                $angle = 45;
                break;
            case 'E':
                $angle = 90;
                break;
            case 'SE':
                $angle = 135;
                break;
            case 'S':
                $angle = 180;
                break;
            case 'SW':
                $angle = 225;
                break;
            case 'W':
                $angle = 270;
                break;
            case 'NW':
                $angle = 315;
                break;

            default:
                break;
        }
    
        ?>

        <div class="row mb-3">
            <div class="card col-6">
                <div class="card-body">
                    <img class="float-left" src="images/wi-wind.svg" width="200" height="200" style="transform:rotate(<?php echo $angle . 'deg'?>);">
                    <h1 class="float-right"><?php echo $vent?> km/h</h1>
                </div>
                <div class="card-footer text-muted">
                    - Direction du vent
                </div>
            </div>

            <div class="card col-6">
                <div class="card-body">
                    <img class="float-left" src="images/power.png" width="200" height="200">
                    <div class="float-right">
                        <h1 class=""><?php echo $puissance?> mW </h1>
                        <h3 class=""><?php echo $tension?> mV</h3>
                        <h3 class=""><?php echo $courant?> mA</h3>
                    </div>      
                </div>
                <div class="card-footer text-muted">
                    - Production
                </div>
            </div>
        </div>

        <div class="row">
            <div class="card col-3">
                <div class="card-body">
                    <img class="float-left" src="images/wi-thermometer.svg" width="100" height="100">
                    <!--<i class="wi wi-thermometer float-left"></i>-->
                    <h3 class="float-right"><?php echo $temperature?>°C</h3>
                </div>
                <div class="card-footer text-muted">
                    - Température
                </div>
            </div>

            <div class="card col-3">
                <div class="card-body">
                    <img class="float-left" src="images/wi-humidity.svg" width="100" height="100">
                    <h3 class="float-right"><?php echo $humidite?> %</h3>
                </div>
                <div class="card-footer text-muted">
                    - Humidité
                </div>
            </div>

            <div class="card col-3">
                <div class="card-body">
                    <img class="float-left" src="images/wi-day-sunny.svg" width="100" height="100">
                    <h3 class="float-right"><?php echo $luminosite?> lux</h3>
                </div>
                <div class="card-footer text-muted">
                    - Luminosité
                </div>
            </div>

            <div class="card col-3">
                <div class="card-body">
                    <img class="float-left" src="images/wi-raindrop.svg" width="100" height="100">
                    <h3 class="float-right"><?php echo $pluie?> mm/h</h3>
                </div>
                <div class="card-footer text-muted">
                    - Pluie
                </div>
            </div>
        </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/holderjs@2.9.4/holder.js"></script>

	<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32" preserveAspectRatio="none" style="display: none; visibility: hidden; position: absolute; top: -100%; left: -100%;"><defs><style type="text/css"></style></defs><text x="0" y="2" style="font-weight:bold;font-size:2pt;font-family:Arial, Helvetica, Open Sans, sans-serif">32x32</text></svg>
</body>
</html>

<script>
    window.setInterval('refresh()', 4000);

    function refresh() {
        window.location.reload();
    }
</script>