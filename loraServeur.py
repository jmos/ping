import serial
import time
import loraLib as lora
import simplejson as json
from threading import *
import mysql.connector
import bson

mode_reception = False
changement_eolienne = False
fin = False

def isInt(x):
    try:
        int(x)
        return True
    except ValueError:
        return False


class ReceptionData(Thread):

	def __init__(self):
		Thread.__init__(self)
		self.sqlInsert = "INSERT INTO mesures (date, vent_direction, vent, encodeur, humidite, luminosite, " \
						 	"pluie, temperature, puissance, courant, tension) " \
						 "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
		"""
		self.sqlInsert = ("INSERT INTO mesures (date, vent_direction, vent, encodeur, humidite, luminosite,"
								"pluie, temperature, puissance, courant, tension, en_fonctionnement) "
						  "VALUES (%(date)s, %(vent_direction)s, %(vent)s, %(encodeur)s, %(humidite)s, %(luminosite)s,"
							  "%(pluie)s, %(temperature)s, %(puissance)s, %(courant)s, %(tension)s)")
		"""
		self.flag_fin = Event()

	def run(self):
		while not self.flag_fin.is_set():
			if globals()['mode_reception'] is True:

				while globals()['changement_eolienne'] == True:
					#envoi de la commande du servo en en cours
					pass

				data = lora.CONST_DATA_SWITCH_MODE
				lora.stopRX()
				lora.TX(data)

				lora.modeRX()
				msg = ''
				while True:
					line = port.readline()
					if not line:
						continue

					try:
						msg = line.decode().split(',')[4]
						msg = msg.replace('\r', '')
						msg = msg.replace('\n', '')
						msg = bytearray.fromhex(msg).decode()
						break
					except IndexError:
						# ligne \r\n
						continue
					except UnicodeDecodeError:
						continue
					except ValueError:
						# ligne \r\n
						continue

				msg = msg.replace('.', ',')

				msg_list = list(msg)

				for x in range(len(msg_list)):
					if msg_list[x] == ',':
						if isInt(msg_list[x - 1]):
							if isInt(msg_list[x + 1]):
								msg_list[x] = '.'

				msg = ''.join(msg_list)
				msg = '{' + msg + '}'

				try:
					data_json = json.loads(msg)
				except json.errors.JSONDecodeError:
					print("Erreur décodage JSON")
				else:
					print(json.dumps(data_json, sort_keys=True, indent=4))

				values_tuple = (data_json['d'], data_json['g'], data_json['v'], data_json['e'], data_json['h'], data_json['l'],
									data_json['p'], data_json['t'], data_json['w'], data_json['i'], data_json['u'])

				curseur.execute(self.sqlInsert, values_tuple)

				globals()['mode_reception'] = False

				"""
				msg = msg.replace('.', ',')

				msg_list = list(msg)

				for x in range(len(msg_list)):
					if msg_list[x] == ',':
						if isInt(msg_list[x - 1]):
							if isInt(msg_list[x + 1]):
								msg_list[x] = '.'

				msg = ''.join(msg_list)
				msg = '{' + msg + '}'

				try:
					data_json = json.loads(msg)
				except json.errors.JSONDecodeError:
					print("Erreur décodage JSON")
				else:
					print(json.dumps(data_json, sort_keys=True, indent=4))
				"""

				"""
					data_dict = {}
					for key, value in data_json.items():
						data_dict[key] = value

					curseur.execute(self.sqlInsert, data_dict)
				"""

class UpdateCapteurs(Thread):

	def __init__(self):
		Thread.__init__(self)
		self.sqlSelect = 'SELECT update_demande FROM flag WHERE id=1'
		self.flag_fin = Event()

	def run(self):
		while not self.flag_fin.is_set():
			curseur3.execute(self.sqlSelect)
			resultat = curseur3.fetchone()
			if resultat[0] == 1:
				globals()['mode_reception'] = True
				# remise à 0 du flag
				curseur3.execute('UPDATE flag SET update_demande = 0 WHERE id = 1')
				resultat[0] == 0
			else:
				continue

			time.sleep(0.5)


class ChangementFonctionnementEolienne(Thread):

	def __init__(self):
		Thread.__init__(self)
		self.sqlSelect = 'SELECT fonctionnement FROM flag WHERE id=1'
		self.flag_fin = Event()
		self.precedent = 1

	def run(self):
		curseur5.execute(self.sqlSelect)
		resultat = curseur5.fetchone()
		self.precedent = resultat[0]

		while not self.flag_fin.is_set():
			curseur5.execute(self.sqlSelect)
			resultat = curseur5.fetchone()

			if resultat[0] != self.precedent:
				#envoie servo COMMANDE resultat[0]
				print("*** Changement de fonctionnement de l'éolienne demandé")

				globals()['changement_eolienne'] = True
				lora.stopRX()
				lora.TX('eolienne:' + str(resultat[0]))
				globals()['changement_eolienne'] = False

				self.precedent = resultat[0]
			else:
				#wallou
				continue

			time.sleep(0.5)


def timerUpdateCapteurs():
	if globals()['fin'] != True:
		curseur4.execute('SELECT update_temps FROM flag WHERE id=1');
		timer_temps = curseur4.fetchone()[0]

		print('timer (re)lancé : ' + str(timer_temps) + 'sec')
		t = Timer(timer_temps, timerUpdateCapteurs)
		t.start()
		globals()['mode_reception'] = True


bdd = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd.autocommit = True

bdd2 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd2.autocommit = True

bdd3 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd3.autocommit = True

bdd4 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd4.autocommit = True

bdd5 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd5.autocommit = True

curseur = bdd.cursor(buffered=True)
curseur2 = bdd2.cursor(buffered=True)
curseur3 = bdd3.cursor(buffered=True)
curseur4 = bdd4.cursor(buffered=True)
curseur5 = bdd5.cursor(buffered=True)

timer_temps = 60


try:
	port = serial.Serial('/dev/ttyUSB1', 38400)
	print("Port {} ouvert".format(port.name))
	print("*****************************")

	lora.init(port)
	timerUpdateCapteurs()
	globals()['mode_reception'] = False  # n'attend pas x secondes la 1ere fois pcq timerInit() change la valeur de mode_reception

	reception_data = ReceptionData()
	update_capteurs = UpdateCapteurs()
	changement_fonctionnement_eolienne = ChangementFonctionnementEolienne()

	reception_data.start()
	update_capteurs.start()
	changement_fonctionnement_eolienne.start()

	#garde le main en vie
	while True:
		time.sleep(0.2)

except KeyboardInterrupt:
	port.close()
	print("\n*****************************")
	print("Port fermé")
	globals()['fin'] = True

