import time
import serial

port = serial.Serial()

CONST_DATA_SWITCH_MODE = 'change'


def reset(port):
    cmd = "AT+GA=DIND,1,8401\r\n"
    port.write(cmd.encode('ascii'))

    time.sleep(3)


def changeFrequenceTX():
    cmd = "AT+RFTX=SET,LORA,858100000,14,125000,7,1,true,8,0,false,false,0,0,0,5,false,true\r\n"
    port.write(cmd.encode('ascii'))
    time.sleep(0.2)


def changeFrequenceRX():
    cmd = "AT+RFRX=SET,LORA,858100000,125000,7,1,true,false,false,10000000,5,true\r\n"
    port.write(cmd.encode('ascii'))
    time.sleep(0.2)


def init(port):
    reset(port)

    cmd = "AT+RF=?\r\n"
    port.write(cmd.encode('ascii'))
    time.sleep(0.2)
    cmd = 'AT+RF=ON\r\n'
    port.write(cmd.encode('ascii'))
    time.sleep(0.2)
    globals()['port'] = port


def TX(msg):
    cmd = 'AT+RFTX=SNDTXT,' + str(msg) + '\r\n'
    print('Message envoyé : ', cmd)
    port.write(cmd.encode('ascii'))


def stopRX():
    print('RX off')
    port.write('AT+RFRX=STOP\r\n'.encode('ASCII'))
    time.sleep(0.2)

    changeFrequenceTX()


def stopTX():
    print('TX off')
    port.write('AT+RFTX=STOP\r\n'.encode('ASCII'))
    time.sleep(0.2)


def modeRX():
    print('RX on')
    stopTX()

    changeFrequenceRX()

    #port.write('AT+RFRX=?\r\n'.encode('ASCII'))
    #time.sleep(0.2)

    port.write('AT+RFRX=CONTRX\r\n'.encode('ASCII'))
    time.sleep(0.2)

