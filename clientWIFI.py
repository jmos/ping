import socket
import errno
import time
import json
from datetime import datetime
from threading import Thread
import threading
import signal
import pickle

HOTE = "10.0.0.1"
PORT = 8001

fin = 0

update_capteurs = 0

class FinProgramme(Exception):
    pass


def service_exit(signum, frame):
    print('\nSignal de fin attrapé. Fermeture de la connexion en cours...')
    raise FinProgramme

class EnvoieData(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.flag_fin = threading.Event()

    def run(self):
        while not self.flag_fin.is_set():

            while globals()['update_capteurs'] != 1:
                continue

            with open('/home/pi/Desktop/donneesCapteurs', 'rb') as reporting:
                lecture = pickle.Unpickler(reporting)
                donnees_recupere = lecture.load()

            data = json.dumps({"d": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                               "g": donnees_recupere['girouette'],
                               "v": donnees_recupere['vent'],
                               "e": donnees_recupere['encodeur'],
                               "h": donnees_recupere['humidite'],
                               "l": donnees_recupere['luminosite'],
                               "p": donnees_recupere['pluie'],
                               "t": donnees_recupere['temperature'],
                               "w": donnees_recupere['puissance'],
                               "i": donnees_recupere['courant'],
                               "u": donnees_recupere['tension']}, separators=('.', ':'))

            print(data)

            try:
                s.send(data.encode())
            except BrokenPipeError:
                self.fin = 1

            globals()['update_capteurs'] = 0


class ReceptionData(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.flag_fin = threading.Event()

    def run(self):
        while not self.flag_fin.is_set():
            try:
                msg = s.recv(4096)
            except socket.error as e:
                err = e.args[0]
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    print('No data available')
                    #continue
                else:
                    print(e)
            else:
                if msg.decode() == 'fin':
                    globals()['fin'] = 1
                    print('Connexion fermée, fermeture socket...')

                else:
                    #print(msg.decode())
                    if msg.decode() == 'update':
                        print('update demande')
                        globals()['update_capteurs'] = 1
                    else:
                        #print(msg.decode())
                        try:
                            etatEolienne = msg.decode().split(':')[1]
                            #print(etatEolienne)
                            with open('/home/pi/Desktop/eolienne', 'w') as file:
                                if etatEolienne == '0':
                                    print('Eolienne arrêt')
                                    file.write('OFF')
                                elif etatEolienne == '1':
                                    print('Eolienne en marche')
                                    file.write('ON')

                        except IndexError:
                            #quand serveur ferme connexion, erreur d'index au moment de split
                            self.flag_fin.set()


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOTE, PORT))
print("Connexion établie avec le serveur sur le port {}".format(PORT))


signal.signal(signal.SIGTERM, service_exit)
signal.signal(signal.SIGINT, service_exit)

try:
    reception = ReceptionData()
    envoie = EnvoieData()

    reception.start()
    envoie.start()

    while True:
        #garder le main en vie pour pouvoir attraper le signal de fin
        time.sleep(0.2)

        if globals()['fin'] == 1:
            reception.flag_fin.set()
            envoie.flag_fin.set()
            reception.join()
            envoie.join()
            break

except FinProgramme:
    reception.flag_fin.set()
    envoie.flag_fin.set()

    globals()['update_capteurs'] = 1

    reception.join()
    envoie.join()


s.close()
print("Socket fermée")
