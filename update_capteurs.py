# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 09:26:55 2019

@author: Grain-Zotier Emeric

V1
"""

import capteurs
import pickle
import os


girouette = []
temperature = []
luminosite = []
encodeur = []
humidite = []
vitesseVent = []
pluie = []
puissance = []
courant = []
tension = []

capteur = capteurs.Capteurs('/dev/ttyUSB0')

def girouette_list():
    a=capteur.girouette()
    girouette.append(a)
    return a


def temperature_list():
    a=capteur.temperature()
    temperature.append(a)
    avg_temperature = sum(temperature) / len(temperature)
    return str(a)
    #return str(round(avg_temperature, 1))


def luminosite_list():
    a=capteur.luminosite()
    luminosite.append(a)
    avg_luminosite = sum(luminosite) / len(luminosite)
    return str(a)
    #return str((int)(avg_luminosite))


def encodeur_list():
    a=capteur.encodeur()
    encodeur.append(a)
    return str(a)


def humidite_list():
    a=capteur.humidite()
    humidite.append(a)
    avg_humidite = sum(humidite) / len(humidite)
    return str(a)
    #return str((int)(avg_humidite))


def vitesseVent_list():
    a=capteur.vitesseVent()
    vitesseVent.append(a)
    avg_vitesseVent = sum(vitesseVent) / len(vitesseVent)
    return str(a)
    #return str(round(avg_vitesseVent, 1))


def pluie_list():
    a=capteur.pluie()
    pluie.append(a)
    avg_pluie = sum(pluie) / len(pluie)
    return str(a)
    #return str((int)(avg_pluie))


def puissance_list():
    a=capteur.puissance()
    puissance.append(a)
    avg_puissance = sum(puissance) / len(puissance)
    return str(a)
    #return str(round(avg_puissance, 2))


def tension_list():
    a=capteur.tension()
    tension.append(a)
    avg_tension = sum(tension) / len(tension)
    return str(a)
    #return str(round(avg_tension, 2))


def courant_list():
    a=capteur.courant()
    courant.append(a)
    avg_courant = sum(courant) / len(courant)
    return str(a)
    #return str(round(avg_courant, 2))


while True:
    capteur.update()

    listing = {
        "girouette": girouette_list(),
        "temperature": temperature_list(),
        "luminosite": luminosite_list(),
        "encodeur": encodeur_list(),
        "humidite": humidite_list(),
        "vent": vitesseVent_list(),
        "pluie": pluie_list(),
        "puissance": puissance_list(),
        "tension" : tension_list(),
        "courant" : courant_list()
    }

    print(listing)

    #change direction de l'éolienne en fonction de girouette
    capteur.servoDirection(listing['girouette'])

    with open('/home/pi/Desktop/donneesCapteurs', 'wb') as reporting:
        enregistrement = pickle.Pickler(reporting)
        enregistrement.dump(listing)
    
    try:
        with open('/home/pi/Desktop/eolienne' , 'r') as file:
            etatEolienne = file.read()
            print('*** servo commande : ' + etatEolienne)
            if etatEolienne == 'ON':
                capteur.servoOnOff(1)
            elif etatEolienne == 'OFF':
                capteur.servoOnOff(0)
    
        os.remove('/home/pi/Desktop/eolienne')
    except FileNotFoundError:
        pass

