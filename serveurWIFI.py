import socket
import mysql.connector
import simplejson as json
from threading import *
import threading
import time
import errno
import signal

PORT = 8001
fin = 0

def isInt(x):
    try:
        int(x)
        return True
    except ValueError:
        return False

class ReceptionData(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.sqlInsert = "INSERT INTO mesures (date, vent_direction, vent, encodeur, humidite, luminosite, " \
						 	"pluie, temperature, puissance, courant, tension) " \
						 "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        self.flag_fin = threading.Event()

    def run(self):
        while not self.flag_fin.is_set():
            try:
                msg = client.recv(4096)
            except socket.error as e:
                err = e.args[0]
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    print("No data available")

            else:
                msg = msg.decode().replace('.', ',')

                msg_list = list(msg)

                for x in range(len(msg_list)):
                    if msg_list[x] == ',':
                        if isInt(msg_list[x - 1]):
                            if isInt(msg_list[x + 1]):
                                msg_list[x] = '.'

                msg = ''.join(msg_list)
                #msg = '{' + msg + '}'

                try:
                    data_json = json.loads(msg)
                    values_tuple = (
                        data_json['d'], data_json['g'], data_json['v'], data_json['e'], data_json['h'], data_json['l'],
                        data_json['p'], data_json['t'], data_json['w'], data_json['i'], data_json['u'])

                    curseur.execute(self.sqlInsert, values_tuple)
                except json.errors.JSONDecodeError:
                    print("Erreur décodage JSON")
                    globals()['receptionOK'] = False
                    time.sleep(2)
                else:
                    print(json.dumps(data_json, sort_keys=True, indent=4))




class ChangementFonctionnementEolienne(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.sqlSelect = 'SELECT fonctionnement FROM flag WHERE id=1'
        self.flag_fin = threading.Event()
        self.precedent = 1

    def run(self):
        curseur2.execute(self.sqlSelect)
        resultat = curseur2.fetchone()
        self.precedent = resultat[0]

        while not self.flag_fin.is_set():

            curseur2.execute(self.sqlSelect)
            resultat = curseur2.fetchone()

            if resultat[0] != self.precedent:
                # envoie servo COMMANDE resultat[0]
                print("*** Changement de fonctionnement de l'éolienne demandé")

                globals()['changement_eolienne'] = True
                try:
                    client.send(('eolienne:' + str(resultat[0])).encode())
                except BrokenPipeError:
                    print('broken piep error')
                    globals()['envoieOK'] = False
                globals()['changement_eolienne'] = False

                self.precedent = resultat[0]
            else:
                # wallou
                continue

            time.sleep(0.5)

class UpdateCapteurs(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.sqlSelect = 'SELECT update_demande FROM flag WHERE id=1'
        self.flag_fin = threading.Event()

    def run(self):
        while not self.flag_fin.is_set():
            curseur3.execute(self.sqlSelect)
            resultat = curseur3.fetchone()

            if resultat[0] == 1:
                msg = 'update'
                print('*** refresh capteur demandé sur IHM')
                #remise à 0 du flag
                curseur3.execute('UPDATE flag SET update_demande = 0 WHERE id = 1')
            else:
                continue

            try:
                client.send(msg.encode())
            except BrokenPipeError:
                print('broken piep error')
                globals()['envoieOK'] = False

            time.sleep(0.5)

class FinProgramme(Exception):
    pass


def service_exit(signum, frame):
    print('\nSignal de fin attrapé. Fermeture du serveur en cours...')
    globals()['fin'] = 1
    raise FinProgramme


bdd = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd.autocommit = True

bdd2 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd2.autocommit = True

bdd3 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd3.autocommit = True

bdd4 = mysql.connector.connect(
  host="localhost",
  database="projet",
  user="root",
  passwd="raspberry"
)
bdd4.autocommit = True

curseur = bdd.cursor(buffered=True)
curseur2 = bdd2.cursor(buffered=True)
curseur3 = bdd3.cursor(buffered=True)
curseur4 = bdd4.cursor(buffered=True)


def timerUpdateCapteurs():
    if globals()['fin'] == 0:
        curseur4.execute('SELECT update_temps FROM flag WHERE id=1');
        timer_temps = curseur4.fetchone()[0]

        t = Timer(timer_temps, timerUpdateCapteurs)
        t.start()
        globals()['mode_reception'] = True

        try:
            client.send('update'.encode())
        except BrokenPipeError:
            print('ici')
            pass
        else:
            print('timer (re)lancé')


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', PORT))
s.listen(5)
s.setblocking(False)
print("Connexion ouverte sur le port {}".format(PORT))

signal.signal(signal.SIGTERM, service_exit)
signal.signal(signal.SIGINT, service_exit)

clientConnecte = False

try:
    while True:
        while True:
            try:
                client, infos = s.accept()
                break
            except BlockingIOError:
                print("En attente du client")
                time.sleep(1)
            #else:
            #    client.setblocking(False)

        clientConnecte = True
        receptionOK = True
        envoieOK = True

        print('=============================================')
        print('         Connexion établie avec ', infos[0])
        print('=============================================')

        timerUpdateCapteurs()

        reception_data = ReceptionData()
        changement_fonctionnement_eolienne = ChangementFonctionnementEolienne()
        update_capteurs = UpdateCapteurs()

        reception_data.start()
        changement_fonctionnement_eolienne.start()
        update_capteurs.start()

        while clientConnecte == True:
            if  receptionOK == False: # and envoieOK == False
                print('=============================================')
                print('             Client déconnecté               ')
                print('=============================================')
                clientConnecte = False
                # fermeture des threads
                reception_data.flag_fin.set()
                changement_fonctionnement_eolienne.flag_fin.set()
                update_capteurs.flag_fin.set()

            #garder le main en vie pour pouvoir attraper le signal de fin
            time.sleep(0.2)

except FinProgramme:
    reception_data.flag_fin.set()
    changement_fonctionnement_eolienne.flag_fin.set()
    update_capteurs.flag_fin.set()

    reception_data.join()
    changement_fonctionnement_eolienne.join()
    update_capteurs.join()
    globals()['timer'].join()

try:
    client.send('fin'.encode())
    client.close()
    print('Connexion client fermée')
except BrokenPipeError:
    #le client s'est déjà déconnecté => fermeture socket
    pass
s.close()
print("Socket fermée")
