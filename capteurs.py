import serial
import time

class Capteurs():

    port = serial.Serial()

    def __init__(self, p):
        self.port = serial.Serial(p, 9600)
        self.port.timeout = 0.1
        print('Port {} ouvert'.format(self.port.name))
        print('*****************************')


    def update(self):
        a="3;"
        self.port.write(a.encode('ascii'))
        time.sleep(2)
        l = self.port.readline()


    def valeurCapteur(self, cmd):
        self.port.write(cmd.encode())
        line = self.port.readline().decode()

        line = line.replace(';\r\n', '')

        return line


    def girouette(self):
        x = self.valeurCapteur('g;')
        try:
            valeur = ((float(x) * 5) / 1023)
        except ValueError:
            return 'NE'

        if (valeur <= 0.45):
            return 'E'
        elif (valeur > 0.45) and (valeur <= 1.40):
            return 'S'
        elif (valeur > 1.40) and (valeur <= 3.84):
            return 'N'
        elif (valeur > 3.84) and (valeur < 4.62):
            return 'W'
        else:
            return 'NE'


    def temperature(self):
        x = self.valeurCapteur('t;')
        try:
            #prochaine ligne pas OP
            x = float(x)
            return x
        except ValueError:
            return -1

        return -2


    def luminosite(self):
        x = self.valeurCapteur('l;')
        try:
            x = int(x)
            #traitement
        except ValueError:
            x = -1

        return x


    def encodeur(self):
        x = self.valeurCapteur('e;')
        try:
            x = int(x)
        except ValueError:
            x = -1

        return x


    def humidite(self):
        x = self.valeurCapteur('h;')
        try:
            x = float(x)
            #traitement
        except ValueError:
            x = -1

        return x


    def vitesseVent(self):
        x = self.valeurCapteur('w;')
        try:
            x = float(x)
            #traitement
        except ValueError:
            x = -1

        return x


    def pluie(self):
        x = self.valeurCapteur('r;')
        try:
            x = float(x)
            #traitement
        except ValueError:
            x = -1

        return x


    def puissance(self):
        x = self.valeurCapteur('p;')
        try:
            x = float(x)
            #traitement
        except ValueError:
            x = -1.0

        return x

    def tension(self):
        x = self.valeurCapteur('v;')
        try:
            x = float(x)
            #traitement
        except ValueError:
            x = -1.0

        return x

    def courant(self):
        x = self.valeurCapteur('c;')
        try:
            x = float(x)
            # traitement
        except ValueError:
            x = -1.0

        return x

    def servoOnOff(self, x):
        if x == 1:
            cmd = '1:1,90;'
        elif x == 0:
            cmd = '1:1,27;'

        self.port.write(cmd.encode())
        l = self.port.readline()

    def servoDirection(self, x):
        if x == 'N':
            cmd = '1:2,120;'
        elif x == 'E':
            cmd = '1:2,180;'
        elif x == 'W':
            cmd = '1:2,60;'
        elif x == 'S':
            cmd = '1:2,0;'

        else :
            cmd = '1:2,0;'

        self.port.write(cmd.encode())
        l = self.port.readline()

    def close(self):
        self.port.close()
        print('*****************************')
        print('Port fermé')

"""
capt = Capteurs('/dev/ttyUSB0')

try:
    while True:
        capt.update()

        print(capt.girouette())
        print(capt.luminosite())
        print(capt.temperature())
        print(capt.encodeur())
        print(capt.humidite())
        print(capt.vitesseVent())
        print(capt.pluie())
        print(capt.puissance())
        print(capt.courant())
        print(capt.tension())
        print('--------------------')


except KeyboardInterrupt:
    capt.close()
"""
